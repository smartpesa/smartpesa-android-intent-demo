package com.smartpesa.mpos.preferences;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import com.smartpesa.mpos.R;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.Date;
import java.util.HashMap;

public class DeviceUsageTracker {
    private static DeviceUsageTracker sInstance;
    private final Context mContext;
    private HashMap<String, Date> mDeviceLastUsedMap;

    public static DeviceUsageTracker getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DeviceUsageTracker(context);
        }
        return sInstance;
    }

    private DeviceUsageTracker(Context context) {
        mContext = context;
        String string = PreferenceHelper.getString(context, R.string.pref_key_device_usage_history, null);
        if (!TextUtils.isEmpty(string)) {
            try {
                mDeviceLastUsedMap = new Gson().fromJson(string, new TypeToken<HashMap<String, Date>>() {
                }.getType());
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        if (mDeviceLastUsedMap == null) {
            mDeviceLastUsedMap = new HashMap<>();
        }
    }

    @Nullable
    public Date getLastUsed(@NonNull String deviceName) {
        if (mDeviceLastUsedMap.isEmpty()) {
            return null;
        }
        return mDeviceLastUsedMap.get(deviceName);
    }

    public DeviceUsageTracker setLastUsed(@NonNull String deviceName, @NonNull Date date) {
        mDeviceLastUsedMap.put(deviceName, date);
        return this;
    }

    public void save() {
        PreferenceHelper.setString(mContext, R.string.pref_key_device_usage_history, new Gson().toJson(mDeviceLastUsedMap));
    }

}
