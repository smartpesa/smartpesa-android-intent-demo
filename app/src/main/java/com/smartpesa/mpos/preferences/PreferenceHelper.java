package com.smartpesa.mpos.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.StringRes;

public class PreferenceHelper {
    public static SharedPreferences get(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static String getString(Context context, @StringRes int keyResId, String defaultValue) {
        return get(context).getString(context.getString(keyResId), defaultValue);
    }

    public static boolean setString(Context context, @StringRes int keyResId, String value) {
        return get(context).edit().putString(context.getString(keyResId), value).commit();
    }

    public static boolean getBoolean(Context context, @StringRes int keyResId, boolean defaultValue) {
        return get(context).getBoolean(context.getString(keyResId), defaultValue);
    }

    public static boolean setBoolean(Context context, int keyResId, boolean value) {
        return get(context).edit().putBoolean(context.getString(keyResId), value).commit();
    }
}
