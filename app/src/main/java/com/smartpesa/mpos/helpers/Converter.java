package com.smartpesa.mpos.helpers;

import com.smartpesa.intent.TransactionType;
import com.smartpesa.intent.result.Card;
import com.smartpesa.intent.result.Currency;
import com.smartpesa.intent.result.TransactionException;
import com.smartpesa.intent.result.TransactionResult;

import android.support.annotation.Nullable;

import smartpesa.sdk.SmartPesa;
import smartpesa.sdk.error.SpCardException;
import smartpesa.sdk.error.SpDeviceConnectionException;
import smartpesa.sdk.error.SpDeviceException;
import smartpesa.sdk.error.SpException;
import smartpesa.sdk.error.SpNetworkException;
import smartpesa.sdk.error.SpPinEntryException;
import smartpesa.sdk.error.SpServerResponseException;
import smartpesa.sdk.error.SpSessionException;
import smartpesa.sdk.error.SpTransactionException;
import smartpesa.sdk.models.version.SpVersionException;

public class Converter {
    public static TransactionResult from(smartpesa.sdk.models.transaction.TransactionResult result) {
        Currency c = Currency.builder()
                .symbol(result.getCurrencySymbol())
                .name(result.getCurrencyName())
                .decimals(result.getCurrencyDecimals())
                .build();

        Card cd = Card.builder()
                .pan(result.getCardNumber())
                .expiry(result.getCardExpiry())
                .holderName(result.getCardHolderName())
                .type(result.getCardType())
                .build();

        return TransactionResult.builder()
                .id(result.getTransactionId())
                .reference(result.getTransactionReference())
                .datetime(result.getTransactionDatetime())
                .amount(result.getAmount())
                .currency(c)
                .card(cd)
                .responseCode(result.getResponseCode())
                .responseDescription(result.getResponseDescription())
                .cvmDescription(result.getCvmDescription())
                .description(result.getTransactionDescription())
                .authorisationResponse(result.getAuthorisationResponse())
                .authorisationResponseCode(result.getAuthorisationResponseCode())
                .isReversed(result.isReversed())
                .type(from(result.getTransactionTypeEnum()))
                .build();
    }

    @Nullable
    public static TransactionType from(SmartPesa.TransactionType type) {
        switch (type) {
            case GOODS_AND_SERVICES:
                return TransactionType.SALES;
            case REFUND_TRANSACTION:
                return TransactionType.REFUND;
            case VOID_ONLINE:
                return TransactionType.VOID;
            default:
                return null;
        }
    }

    @Nullable
    public static SmartPesa.TransactionType from(TransactionType type) {
        switch (type) {
            case SALES:
                return SmartPesa.TransactionType.GOODS_AND_SERVICES;
            case REFUND:
                return SmartPesa.TransactionType.REFUND_TRANSACTION;
            case VOID:
                return SmartPesa.TransactionType.VOID_ONLINE;
            default:
                return null;
        }
    }

    public static TransactionException from(SpException exception) {
        String reason = "";
        if (exception instanceof SpVersionException) {
            reason = ((SpVersionException) exception).getReason().toString();
        } else if (exception instanceof SpNetworkException) {
            reason = ((SpNetworkException) exception).getReason().toString();
        } else if (exception instanceof SpSessionException) {
            reason = "Session Expired.";
        } else if (exception instanceof SpCardException) {
            reason = ((SpCardException) exception).getReason().toString();
        } else if (exception instanceof SpDeviceException) {
            reason = ((SpDeviceException) exception).getReason().toString();
        } else if (exception instanceof SpDeviceConnectionException) {
            reason = ((SpDeviceConnectionException) exception).getReason().toString();
        } else if (exception instanceof SpTransactionException) {
            reason = ((SpTransactionException) exception).getReason().toString();
        } else if (exception instanceof SpServerResponseException) {
            reason = "Server Processing Failed.";
        } else if (exception instanceof SpPinEntryException) {
            reason = ((SpPinEntryException) exception).getReason().toString();
        }
        return new TransactionException(exception, exception.getMessage(), reason, ""); //TODO
    }
}
