package com.smartpesa.mpos;

import com.smartpesa.mpos.preferences.PreferenceHelper;

import android.app.Application;
import android.content.Context;

import smartpesa.sdk.ServiceManager;
import smartpesa.sdk.ServiceManagerConfig;
import smartpesa.sdk.SmartPesa;

public class SmartPesaMPosApplication extends Application {

    private static final String END_POINT = "dev.smartpesa.com";

    @Override
    public void onCreate() {
        super.onCreate();
        Context context = this.getApplicationContext();
        ServiceManager.init(ServiceManagerConfig.newBuilder(context)
                .endPoint(END_POINT)
                .withoutSsl()
                .build());

        //Store settings in preference.
        PreferenceHelper.get(context).edit()
                .putString(getString(R.string.pref_key_endpoint), END_POINT)
                .putString(getString(R.string.pref_key_version), BuildConfig.VERSION_NAME)
                .putString(getString(R.string.pref_key_build_date), SmartPesa.SDK_BUILD_TIME)
                .putString(getString(R.string.pref_key_sdk_version), SmartPesa.SDK_VERSION)
                .putString(getString(R.string.pref_key_sdk_build_date), SmartPesa.SDK_BUILD_TIME)
                .apply();
    }
}
