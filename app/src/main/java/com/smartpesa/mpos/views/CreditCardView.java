package com.smartpesa.mpos.views;

import com.smartpesa.mpos.R;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import smartpesa.sdk.SmartPesa;
import smartpesa.sdk.models.transaction.Bin;
import smartpesa.sdk.models.transaction.Card;

public class CreditCardView extends CardView {
    @BindView(R.id.card_pan)
    TextView mCardPan;

    @BindView(R.id.card_expiry)
    TextView mCardExpiry;

    @BindView(R.id.card_holder_name)
    TextView mCardHolder;

    @BindView(R.id.image_card_brand)
    ImageView mCardBrand;

    @BindView(R.id.image_chip)
    ImageView mChip;

    public CreditCardView(Context context) {
        this(context, null);
    }

    public CreditCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CreditCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.view_card, this);
        ButterKnife.bind(this);
    }

    public void setCard(Card card) {
        mCardPan.setText(card.getCardPan());
        mCardHolder.setText(card.getCardHolderName());
        mCardExpiry.setText(card.getCardExpiry());
        if (card.getBin() != null) {
            setBin(card.getBin());
        }
    }

    public void setMethod(SmartPesa.Method method) {
        if (method == SmartPesa.Method.ICC) {
            mChip.setVisibility(View.VISIBLE);
        } else {
            mChip.setVisibility(View.GONE);
        }
    }

    public void setBin(@NonNull Bin bin) {
        if (bin.getCardIconResId() != 0) {
            mCardBrand.setImageResource(bin.getCardIconResId());
        }
    }

}
