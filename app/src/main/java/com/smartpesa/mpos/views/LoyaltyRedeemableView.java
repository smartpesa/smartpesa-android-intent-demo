package com.smartpesa.mpos.views;

import com.smartpesa.mpos.R;
import com.smartpesa.mpos.helpers.MoneyFormatHelper;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import smartpesa.sdk.models.currency.Currency;
import smartpesa.sdk.models.loyalty.Redeemable;

public class LoyaltyRedeemableView extends RelativeLayout implements Checkable {

    private boolean isChecked;
    @BindView(R.id.redeemable_amount)
    TextView mAmount;
    @BindView(R.id.redeemable_balance)
    TextView mBalance;
    @BindView(R.id.redeemable_checked)
    CheckBox mCheckBox;
    @BindView(R.id.redeemable_name)
    TextView mName;

    public LoyaltyRedeemableView(Context context) {
        this(context, null);
    }

    public LoyaltyRedeemableView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoyaltyRedeemableView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.view_loyalty_redeemable, this);
        ButterKnife.bind(this);
        setDescendantFocusability(FOCUS_BLOCK_DESCENDANTS);
    }

    @Override
    public void setChecked(boolean b) {
        isChecked = b;
        updateUi();
    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public void toggle() {
        isChecked = !isChecked;
        updateUi();
    }

    private void updateUi() {
        if (isChecked) {
            mAmount.setTextColor(mName.getCurrentTextColor());
        } else {
            mAmount.setTextColor(mBalance.getCurrentTextColor());
        }
        mCheckBox.setChecked(isChecked);
    }

    public void setRedeemable(Currency currency, Redeemable redeemable) {
        mAmount.setText(MoneyFormatHelper.getInstance(currency).format(redeemable.getAmount()));
        mName.setText(redeemable.getName());
        mBalance.setText(getContext().getString(R.string.redeemable_balance_format, redeemable.getBalance(), redeemable.getUnit()));
    }
}
