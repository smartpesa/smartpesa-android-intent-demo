package com.smartpesa.mpos.fragments;

import com.smartpesa.mpos.R;
import com.wang.avi.AVLoadingIndicatorView;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import smartpesa.sdk.devices.SpDevice;

public abstract class BluetoothDialogFragment<T extends SpDevice> extends AppCompatDialogFragment {

    private static final String TAG = BluetoothDialogFragment.class.getName();
    protected DeviceSelectedListener<T> mListener;
    protected ArrayAdapter<T> mAdapter;
    protected List<T> mData = new ArrayList<>();
    @Nullable private OnCancelListener mCancelListener;

    @BindView(R.id.list)
    ListView mListView;
    @BindView(R.id.negative_button)
    Button mNegativeButton;
    @BindView(R.id.bluetooth_scan_status)
    AVLoadingIndicatorView mIndicatorView;
    @BindView(R.id.remember_mpos)
    CheckBox mRemember;

    @NonNull
    protected abstract ArrayAdapter<T> onCreateAdapter();

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setTitle(R.string.select_pesapod);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mAdapter = onCreateAdapter();
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mListener.onSelected(mData.get(position), mRemember.isChecked());
                dismiss();
            }
        });

        mNegativeButton.setVisibility(View.VISIBLE);
        mNegativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().cancel();
            }
        });
    }

    public void setIsScanning(boolean isScanning) {
        if (isScanning) {
            mIndicatorView.smoothToShow();
        } else {
            mIndicatorView.smoothToHide();
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(TAG, "onCancel: Dialog");
        if (mCancelListener != null) {
            mCancelListener.onCancel(dialog);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(TAG, "onDismiss: Dialog");
    }

    public void updateDevices(Collection<T> devices) {
        mData.clear();
        mData.addAll(devices);
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void setSelectedListener(DeviceSelectedListener<T> listener) {
        mListener = listener;
    }

    public void setCancelListener(OnCancelListener cancelListener) {
        mCancelListener = cancelListener;
    }

    public interface DeviceSelectedListener<T> {
        void onSelected(T device, boolean shouldRemember);
    }

    public interface OnCancelListener {
        void onCancel(DialogInterface dialog);
    }
}