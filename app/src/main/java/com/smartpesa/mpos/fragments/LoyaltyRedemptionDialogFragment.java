package com.smartpesa.mpos.fragments;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import com.smartpesa.mpos.R;
import com.smartpesa.mpos.helpers.MoneyFormatHelper;
import com.smartpesa.mpos.views.LoyaltyRedeemableView;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import smartpesa.sdk.ServiceManager;
import smartpesa.sdk.models.currency.Currency;
import smartpesa.sdk.models.loyalty.LoyaltyTransaction;
import smartpesa.sdk.models.loyalty.Redeemable;
import smartpesa.sdk.models.merchant.VerifiedMerchantInfo;

public class LoyaltyRedemptionDialogFragment extends AppCompatDialogFragment {

    private static final String TAG = LoyaltyRedemptionDialogFragment.class.getName();
    private static final String KEY_LOYALTY_TRANSACTION = LoyaltyRedemptionDialogFragment.class.getName() + ".loyalty.transaction";
    @BindView(R.id.loyalty_discount_amount)
    TextView mDiscountAmount;
    @BindView(R.id.loyalty_final_amount)
    TextView mFinalAmount;
    @BindView(R.id.loyalty_original_amount)
    TextView mOriginalAmount;
    @BindView(R.id.loyalty_redeemable_list)
    ListView mRedeemableList;
    @BindView(R.id.positive_button)
    Button mPositiveButton;
    @BindView(R.id.negative_button)
    Button mNegativeButton;
    private Listener mListener;
    VerifiedMerchantInfo mCachedMerchant;
    private LoyaltyTransaction mLoyaltyTransaction;
    private MoneyFormatHelper mMoneyFormatHelper;

    public static LoyaltyRedemptionDialogFragment newInstance(LoyaltyTransaction loyaltyTransaction) {
        String json = new Gson().toJson(loyaltyTransaction);
        Bundle args = new Bundle();
        args.putString(KEY_LOYALTY_TRANSACTION, json);
        LoyaltyRedemptionDialogFragment fragment = new LoyaltyRedemptionDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setTitle(R.string.loyalty_redemption);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCachedMerchant = ServiceManager.get(getActivity()).getCachedMerchant();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_loyalty_redemption, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LoyaltyRedemptionDialogFragment.Listener) {
            mListener = (Listener) context;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        try {
            mLoyaltyTransaction = new Gson().fromJson(getArguments().getString(KEY_LOYALTY_TRANSACTION, null), LoyaltyTransaction.class);
            BigDecimal originalTransactionAmount = mLoyaltyTransaction.getAdjustedAmount().add(mLoyaltyTransaction.getDiscountAmount());
            mRedeemableList.setAdapter(new RedeemableAdapter(getActivity(), mLoyaltyTransaction.getRedeemableList()));

            mMoneyFormatHelper = MoneyFormatHelper.getInstance(mCachedMerchant.getCurrency());

            mOriginalAmount.setText(mMoneyFormatHelper.format(originalTransactionAmount));
            mDiscountAmount.setText(mMoneyFormatHelper.format(mLoyaltyTransaction.getDiscountAmount()));
            mFinalAmount.setText(mMoneyFormatHelper.format(mLoyaltyTransaction.getAdjustedAmount()));

        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            if (mListener != null) {
                mListener.onCancel();
                getDialog().dismiss();
                return;
            }
        }

        mPositiveButton.setText(R.string.redeem);
        mPositiveButton.setVisibility(View.VISIBLE);
        mNegativeButton.setText(R.string.no_further_redemption);
        mNegativeButton.setVisibility(View.VISIBLE);

        mPositiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener == null) {
                    return;
                }
                if (mRedeemableList.getCheckedItemCount() <= 0) {
                    mListener.onContinue(Collections.<Redeemable>emptyList());
                } else {
                    mListener.onContinue(getCheckedRedeemable());
                }
                getDialog().dismiss();
            }
        });

        mNegativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener == null) {
                    return;
                }
                mListener.onCancel();
                getDialog().dismiss();
            }
        });

        mRedeemableList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                BigDecimal finalAmount = mLoyaltyTransaction.getAdjustedAmount();
                if (mRedeemableList.getCheckedItemCount() > 0) {
                    List<Redeemable> checkedRedeemable = getCheckedRedeemable();
                    if (checkedRedeemable.size() > 0) {
                        for (Redeemable r : checkedRedeemable) {
                            finalAmount = finalAmount.subtract(r.getRedeemed());
                        }
                    }
                }
                mFinalAmount.setText(mMoneyFormatHelper.format(finalAmount));
            }
        });
    }

    private List<Redeemable> getCheckedRedeemable() {

        List<Redeemable> redeemedList = new ArrayList<>();
        SparseBooleanArray checkedItemPositions = mRedeemableList.getCheckedItemPositions();
        for (int i = 0; i < checkedItemPositions.size(); i++) {
            int position = checkedItemPositions.keyAt(i);
            if (checkedItemPositions.get(position)) {
                Redeemable r = (Redeemable) mRedeemableList.getItemAtPosition(position);
                r.redeem(r.getAmount()); //Redeem all amount
                redeemedList.add(r);
            }
        }
        return redeemedList;

    }

    public class RedeemableAdapter extends ArrayAdapter<Redeemable> {

        private Currency currency;

        public RedeemableAdapter(Context context, List<Redeemable> redeemableList) {
            super(context, R.layout.layout_loyalty_redeemable, redeemableList);

            if (mCachedMerchant != null) {
                currency = mCachedMerchant.getCurrency();
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loyalty_redeemable, parent, false);
            }
            if (convertView instanceof LoyaltyRedeemableView) {
                ((LoyaltyRedeemableView) convertView).setRedeemable(currency, getItem(position));
            }
            return convertView;
        }
    }

    public interface Listener {
        void onContinue(List<Redeemable> redeemedList);

        void onCancel();
    }
}
