package com.smartpesa.mpos.fragments;

import com.smartpesa.mpos.R;
import com.smartpesa.mpos.preferences.DeviceUsageTracker;
import com.smartpesa.mpos.preferences.PreferenceHelper;

import org.ocpsoft.prettytime.PrettyTime;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import smartpesa.sdk.devices.SpTerminal;

public class TerminalDialogFragment extends BluetoothDialogFragment<SpTerminal> {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_bluetooth_list, container, false);
    }

    @NonNull
    @Override
    protected ArrayAdapter<SpTerminal> onCreateAdapter() {
        return new TerminalDialogFragment.BluetoothAdapter(getActivity(), mData);
    }

    protected static class BluetoothAdapter extends ArrayAdapter<SpTerminal> {
        private final String mPreferredDeviceName;
        private DeviceUsageTracker mDeviceUsageTracker;
        private PrettyTime mPrettyTime = new PrettyTime();

        public BluetoothAdapter(Context context, List<SpTerminal> objects) {
            super(context, R.layout.layout_mpos_selection, objects);
            mDeviceUsageTracker = DeviceUsageTracker.getInstance(context);
            mPreferredDeviceName = PreferenceHelper.getString(context, R.string.pref_key_preferred_device, null);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_mpos_selection, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            SpTerminal device = getItem(position);

            holder.serialNumber.setText(device.getName());

            Date lastUsed = mDeviceUsageTracker.getLastUsed(device.getName());
            if (lastUsed != null) {
                holder.lastUsed.setText(convertView.getContext().getString(R.string.last_used_format, mPrettyTime.format(lastUsed)));
            } else {
                holder.lastUsed.setText("");
            }

            switch (device.getDeviceType()) {
                case PESA_POD:
                    holder.deviceType.setText(getContext().getString(R.string.pref_model_pod));
                    holder.deviceIcon.setImageResource(R.drawable.ic_pesa_pod);
                    break;
                case PESA_POD_2:
                    holder.deviceType.setText(getContext().getString(R.string.pref_model_pod2));
                    holder.deviceIcon.setImageResource(R.drawable.ic_pesa_pod_2);
                    break;
                case PESA_POD_2_PLUS:
                    holder.deviceType.setText(getContext().getString(R.string.pref_model_pod2plus));
                    holder.deviceIcon.setImageResource(R.drawable.ic_pesa_pod_2_plus);
                    break;
                case PESA_PAX:
                    holder.deviceType.setText(R.string.pref_model_pax);
                    holder.deviceIcon.setImageResource(R.drawable.ic_pesapax);
                    break;
                default:
                    holder.deviceType.setText(getContext().getString(R.string.unknown));
                    holder.deviceIcon.setImageResource(R.drawable.ic_pesa_pod);
            }
            return convertView;
        }

        public static class ViewHolder {
            @BindView(R.id.device_serial_number)
            TextView serialNumber;
            @BindView(R.id.device_type)
            TextView deviceType;
            @BindView(R.id.device_last_used)
            TextView lastUsed;
            @BindView(R.id.device_icon)
            ImageView deviceIcon;

            public ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }
    }
}
