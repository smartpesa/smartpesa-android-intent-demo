package com.smartpesa.mpos.activities;

import com.smartpesa.mpos.R;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.MenuItem;

import java.util.List;

public class SettingsActivity extends AppCompatPreferenceActivity {
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            if (value != null) {
                if(preference instanceof ListPreference){
                    int index = ((ListPreference) preference).findIndexOfValue(value.toString());
                    CharSequence entry = ((ListPreference) preference).getEntries()[index];
                    preference.setSummary(entry);
                }else{
                    String stringValue = value.toString();
                    // Set the summary to the value's
                    // simple string representation.
                    preference.setSummary(stringValue);
                }
            }
            return true;
        }
    };

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), null));
    }

    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || AccountPreferenceFragment.class.getName().equals(fragmentName)
                || AboutPreferenceFragment.class.getName().equals(fragmentName)
                || TransactionPreferenceFragment.class.getName().equals(fragmentName)
                || DevicePreferenceFragment.class.getName().equals(fragmentName);
    }

    public static class SettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
            ((AppCompatPreferenceActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                getActivity().onBackPressed();
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class AccountPreferenceFragment extends SettingsFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_account);

            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_merchant_code)));
            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_operator_code)));
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class DevicePreferenceFragment extends SettingsFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_device);

            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_preferred_device_model)));
            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_bluetooth_scanning_millis)));
            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_preferred_device)));
        }
    }

    public static class TransactionPreferenceFragment extends SettingsFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_transaction);

            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_preferred_card_mode)));
            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_default_from_account_type)));
        }
    }

    public static class AboutPreferenceFragment extends SettingsFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_about);

            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_endpoint)));
            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_version)));
            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_build_date)));
            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_sdk_version)));
            bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_key_sdk_build_date)));
        }
    }
}
