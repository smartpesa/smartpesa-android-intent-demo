package com.smartpesa.mpos.activities;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import com.smartpesa.intent.TransactionArgument;
import com.smartpesa.intent.result.TransactionError;
import com.smartpesa.intent.result.TransactionException;
import com.smartpesa.intent.result.TransactionResult;
import com.smartpesa.mpos.R;
import com.smartpesa.mpos.fragments.BluetoothDialogFragment;
import com.smartpesa.mpos.fragments.LoyaltyRedemptionDialogFragment;
import com.smartpesa.mpos.fragments.TerminalDialogFragment;
import com.smartpesa.mpos.fragments.TransactionResultDialogFragment;
import com.smartpesa.mpos.helpers.Converter;
import com.smartpesa.mpos.helpers.MoneyFormatHelper;
import com.smartpesa.mpos.preferences.DeviceUsageTracker;
import com.smartpesa.mpos.preferences.PreferenceHelper;
import com.smartpesa.mpos.views.CreditCardView;
import com.wang.avi.AVLoadingIndicatorView;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import smartpesa.sdk.ServiceManager;
import smartpesa.sdk.SmartPesa;
import smartpesa.sdk.devices.SpTerminal;
import smartpesa.sdk.error.SpException;
import smartpesa.sdk.error.SpTransactionException;
import smartpesa.sdk.interfaces.TerminalScanningCallback;
import smartpesa.sdk.interfaces.TransactionCallback;
import smartpesa.sdk.models.loyalty.Loyalty;
import smartpesa.sdk.models.loyalty.LoyaltyTransaction;
import smartpesa.sdk.models.loyalty.Redeemable;
import smartpesa.sdk.models.merchant.VerifiedMerchantInfo;
import smartpesa.sdk.models.merchant.VerifyMerchantCallback;
import smartpesa.sdk.models.transaction.Bin;
import smartpesa.sdk.models.transaction.Card;
import smartpesa.sdk.models.transaction.Transaction;
import smartpesa.sdk.models.version.GetVersionCallback;
import smartpesa.sdk.models.version.Version;

public class MainActivity extends AppCompatActivity implements LoyaltyRedemptionDialogFragment.Listener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = MainActivity.class.getName();
    private static final String BLUETOOTH_FRAGMENT_TAG = "bluetooth_terminal";
    private static final String TRANSACTION_RESULT_DIALOG_TAG = "transaction_result";
    private static final String LOYALTY_REDEMPTION_FRAGMENT_TAG = "loyalty_transaction";
    private static final int REQUEST_LOCATION_PERMISSION = 9182;
    private ServiceManager mServiceManager;

    @BindView(R.id.caption)
    TextView mCaption;
    @BindView(R.id.progress_bar)
    AVLoadingIndicatorView mProgressBar;
    @BindView(R.id.message)
    TextView mMessage;
    @BindView(R.id.transaction_type)
    TextView mType;
    @BindView(R.id.amount)
    TextView mAmount;
    @BindView(R.id.device)
    TextView mDevice;
    @BindView(R.id.card_view)
    CreditCardView mCardView;
    @BindView(R.id.version)
    TextView mVersion;
    @BindView(R.id.battery_status)
    TextView mBatteryStatus;
    @BindView(R.id.merchant)
    TextView mMerchantLabel;
    @BindView(R.id.original_amount)
    TextView mOriginalAmount;
    @BindView(R.id.main_container)
    View mContainer;
    private TransactionArgument mTransactionArgument;

    private String mMerchantCode;
    private String mOperatorCode;
    private String mOperatorPin;
    private boolean mAlwaysNewSession;
    private boolean mRememberDevice;
    private boolean mAutoSelectMPos;
    private String mPreferredDevice;
    private SmartPesa.AccountType mFromAccountType;
    private MoneyFormatHelper mMoneyFormatHelper;
    private LoyaltyTransaction mLoyaltyTransaction;
    private boolean mIsLoyaltyEnabled;
    private Snackbar mSnackbar;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //Get transaction args.
        Intent intent = getIntent();
        if (intent == null) {
            TransactionException e = new TransactionException(getString(R.string.error_argument_invalid), getString(R.string.error_argument_invalid_caption), "");
            showError(e);
            return;
        }

        // Location request argument
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setFastestInterval(60000)
                .setInterval(3600000);

        mTransactionArgument = intent.getParcelableExtra(com.smartpesa.intent.SmartPesa.KEY_TRANSACTION_ARGUMENT);
        String callingSdkVersion = intent.getStringExtra(com.smartpesa.intent.SmartPesa.KEY_SDK_VERSION);
        mVersion.setText(getString(R.string.sdk_version_format, callingSdkVersion, SmartPesa.SDK_VERSION));

        String[] checkPermissions = checkPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);
        if (checkPermissions.length > 0) {
            askForPermissions(checkPermissions);
        } else {
            permissionGranted();
        }

    }

    private void permissionGranted() {
        if (mTransactionArgument == null) {
            TransactionException e = new TransactionException(getString(R.string.error_argument_invalid), getString(R.string.error_argument_invalid_caption), "");
            showError(e);
            return;
        }

        // Create instance of GoogleApiClient
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        mOriginalAmount.setPaintFlags(mOriginalAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        mType.setText(R.string.trasaction_type_sales);

        //Get merchant credentials which has been set.
        mMerchantCode = PreferenceHelper.getString(this, R.string.pref_key_merchant_code, null);
        mOperatorCode = PreferenceHelper.getString(this, R.string.pref_key_operator_code, null);
        mOperatorPin = PreferenceHelper.getString(this, R.string.pref_key_operator_pin, null);

        if (TextUtils.isEmpty(mMerchantCode)
                || TextUtils.isEmpty(mOperatorCode)
                || TextUtils.isEmpty(mOperatorPin)) {
            // Merchant credentials not yet set.
            // Improve this by prompting the merchant's credentials directly.
            TransactionException e = new TransactionException(getString(R.string.error_credential_not_set), getString(R.string.error_credential_caption), "");
            showError(e);
        } else {
            // Set default preference values
            PreferenceManager.setDefaultValues(this, R.xml.pref_device, false);
            PreferenceManager.setDefaultValues(this, R.xml.pref_transaction, false);

            // Get preference values
            mAlwaysNewSession = PreferenceHelper.getBoolean(this, R.string.pref_key_new_session, false);
            mAutoSelectMPos = PreferenceHelper.getBoolean(this, R.string.pref_key_auto_preferred, false);
            mPreferredDevice = PreferenceHelper.getString(this, R.string.pref_key_preferred_device, null);
            mFromAccountType = getPreferenceFromAccType();
            mIsLoyaltyEnabled = PreferenceHelper.getBoolean(this, R.string.pref_key_is_loyalty_enabled, false);

            mServiceManager = ServiceManager.get(this.getApplicationContext());

            // Get extra arg from merchant's preference.
            VerifiedMerchantInfo cachedMerchant = mServiceManager.getCachedMerchant();
            if (cachedMerchant != null &&
                    credentialMatches(cachedMerchant)) {
                // We have a cached merchant which is the same as the one sets in the login.
                initTransaction();
            } else {
                getVersion();
            }

        }
    }

    private void askForPermissions(String[] checkPermissions) {
        ActivityCompat.requestPermissions(this, checkPermissions, REQUEST_LOCATION_PERMISSION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                boolean allGranted = true;
                for (int i = 0; i < permissions.length; i++) {
                    int result = grantResults[i];

                    if (result != PackageManager.PERMISSION_GRANTED) {
                        allGranted = false;
                    }
                }
                if (allGranted) {
                    permissionGranted();
                } else {
                    showError(new TransactionException("Location Permission Denied", "Location permission denied by user. SmartPesa requires location permission to be able to start transaction", ""));
                }
        }
    }

    private String[] checkPermissions(String... permissions) {
        ArrayList<String> deniedPermissions = new ArrayList<>();
        for (String permission : permissions) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, permission);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                deniedPermissions.add(permission);
            }
        }
        // Turns out that providing a zero-length array, even creating it and throwing it away,
        // is on average faster than allocating an array of the right size.
        // For benchmarks and explanation see here: shipilev.net/blog/2016/arrays-wisdom-ancients
        // noinspection ToArrayCallWithZeroLengthArrayArgument
        return deniedPermissions.toArray(new String[0]);
    }

    private void initTransaction() {
        boolean shouldPromptAccType = PreferenceHelper.getBoolean(this, R.string.pref_key_prompt_account_type, false);
        if (shouldPromptAccType) {
            promptForAccType();
        } else {
            startScan();
        }
    }

    private boolean credentialMatches(VerifiedMerchantInfo cachedMerchant) {
        return !mAlwaysNewSession &&
                (cachedMerchant.getMerchantCode() != null &&
                        cachedMerchant.getOperatorCode() != null &&
                        cachedMerchant.getMerchantCode().equals(mMerchantCode) &&
                        cachedMerchant.getOperatorCode().equals(mOperatorCode));
    }

    private void getVersion() {
        mProgressBar.setIndicatorColor(getResources().getColor(R.color.color_primary));
        mProgressBar.smoothToShow();
        setStatus(getString(R.string.authenticating));
        mServiceManager.getVersion(mGetVersionCallback);
    }

    protected void login() {
        mServiceManager.verifyMerchant(mMerchantCode, mOperatorCode, mOperatorPin, mVerifyMerchantCallback);
    }

    private void startScan() {
        setStatus(getString(R.string.scanning));
        VerifiedMerchantInfo merchantInfo = mServiceManager.getCachedMerchant();
        mMoneyFormatHelper = MoneyFormatHelper.getInstance(merchantInfo != null ? merchantInfo.getCurrency() : null);
        mMerchantLabel.setText(merchantInfo != null ? getString(R.string.merchant_name_format, merchantInfo.getMerchantName(), merchantInfo.getOperatorName()) : null);

        mAmount.setText(mMoneyFormatHelper.format(mTransactionArgument.amount()));

        SmartPesa.TerminalScanningParam.Builder builder = new SmartPesa.TerminalScanningParam.Builder();
        long millis = getPreferenceScanningMillis();
        if (millis > 0L) {
            builder.scanTimeoutMillis(millis);
        }
        SmartPesa.DeviceType type = getPreferenceDeviceType();
        if (type != null) {
            builder.deviceType(type);
        }
        mServiceManager.scanTerminal(builder.build(), mScanningCallback);
    }

    private long getPreferenceScanningMillis() {
        long millis = 0L;
        String sm = PreferenceHelper.getString(this, R.string.pref_key_bluetooth_scanning_millis, null);
        if (!TextUtils.isEmpty(sm)) {
            try {
                millis = Long.valueOf(sm);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return millis;
    }

    @Nullable
    private SmartPesa.DeviceType getPreferenceDeviceType() {
        SmartPesa.DeviceType type = null;
        String d = PreferenceHelper.getString(this, R.string.pref_key_preferred_device_model, null);
        if (!TextUtils.isEmpty(d)) {
            try {
                type = SmartPesa.DeviceType.valueOf(d);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return type;
    }

    public void displayBluetoothDevice(Collection<SpTerminal> devices) {
        if (devices.isEmpty()) {
            return;
        }
        TerminalDialogFragment dialog;
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(BLUETOOTH_FRAGMENT_TAG);

        if (mAutoSelectMPos && !TextUtils.isEmpty(mPreferredDevice)) {
            Log.d(TAG, "preferred: " + mPreferredDevice);
            for (SpTerminal terminal : devices) {
                if (terminal.getName() != null && terminal.getName().equals(mPreferredDevice)) {
                    doTransact(terminal);
                    return;
                }
            }
            Log.d(TAG, "preferred device not found");
        }

        if (fragment == null) {
            dialog = new TerminalDialogFragment();
            dialog.show(getSupportFragmentManager(), BLUETOOTH_FRAGMENT_TAG);
        } else {
            dialog = (TerminalDialogFragment) fragment;
        }
        dialog.setCancelListener(new BluetoothDialogFragment.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mServiceManager.stopScan();
                showError(new TransactionException(getString(R.string.canceled), getString(R.string.canceled_scan_caption), ""));
            }
        });
        dialog.setSelectedListener(new DeviceSelectedListenerImpl());
        dialog.updateDevices(devices);
    }

    private void doTransact(SpTerminal device) {
        // UI indicator update
        mProgressBar.setIndicatorColor(getResources().getColor(R.color.bt_blue));
        mProgressBar.smoothToShow();

        SmartPesa.TransactionType transactionType = Converter.from(mTransactionArgument.transactionType());

        SmartPesa.TransactionParam.Build builder = SmartPesa.TransactionParam.newBuilder()
                .transactionType(transactionType)
                .terminal(device)
                .amount(mTransactionArgument.amount());

        if (mTransactionArgument.transactionId() != null) {
            builder.transactionId(mTransactionArgument.transactionId());
        }

        if (mFromAccountType != null) {
            builder.from(mFromAccountType);
        }

        if (mIsLoyaltyEnabled) {
            builder.withLoyalty();
        }

        SmartPesa.CardMode cardMode = getPreferenceCardMode();
        if (cardMode != null) {
            builder.cardMode(cardMode);
        }

        mServiceManager.performTransaction(builder.build(), mTransactionCallback);
    }

    private SmartPesa.CardMode getPreferenceCardMode() {
        SmartPesa.CardMode mode = null;
        String d = PreferenceHelper.getString(this, R.string.pref_key_preferred_card_mode, null);
        if (!TextUtils.isEmpty(d)) {
            try {
                mode = SmartPesa.CardMode.valueOf(d);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return mode;
    }

    private void promptForAccType() {
        new AlertDialog.Builder(this, R.style.AppTheme_Dialog_NoTitle)
                .setTitle(R.string.select_account_type)
                .setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.pref_account_type_options)), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String accType = getResources().getStringArray(R.array.pref_account_type_values)[i];
                        mFromAccountType = getAccountTypeFromString(accType);
                        dialogInterface.dismiss();
                        startScan();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        showError(new TransactionException(getString(R.string.canceled), getString(R.string.canceled_acc_selection_caption), ""));
                    }
                })
                .show();
    }

    private SmartPesa.AccountType getPreferenceFromAccType() {
        SmartPesa.AccountType type;
        String d = PreferenceHelper.getString(this, R.string.pref_key_default_from_account_type, null);
        type = getAccountTypeFromString(d);
        return type;
    }

    @Nullable
    private SmartPesa.AccountType getAccountTypeFromString(String string) {
        SmartPesa.AccountType type = null;
        if (!TextUtils.isEmpty(string)) {
            try {
                type = SmartPesa.AccountType.valueOf(string);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return type;
    }

    private void setMessage(String message) {
        mCaption.setText(message);
    }

    private void setStatus(String message) {
        mMessage.setText(message);
    }

    private void showError(TransactionException exception) {
        Intent intent = new Intent();
        intent.putExtra(com.smartpesa.intent.SmartPesa.KEY_TRANSACTION_ERROR, TransactionError.builder().transactionException(exception).build());
        setResult(RESULT_CANCELED, intent);

        TransactionResultDialogFragment tf = TransactionResultDialogFragment.newInstance(exception, getFinalTransactedAmount());
        showResultDialog(tf);
    }

    private BigDecimal getFinalTransactedAmount() {
        if (mLoyaltyTransaction != null) {
            return mLoyaltyTransaction.getAdjustedAmount();
        } else if (mTransactionArgument != null) {
            return mTransactionArgument.amount();
        } else {
            //Error? show 0 as transacted.
            return BigDecimal.ZERO;
        }
    }

    private GetVersionCallback mGetVersionCallback = new GetVersionCallback() {

        @Override
        public void onSuccess(Version version) {
            login();
        }

        @Override
        public void onError(SpException exception) {
            clearPrompt();
            showError(Converter.from(exception));
        }
    };

    private void clearPrompt() {
        if (mSnackbar != null) {
            mSnackbar.dismiss();
            mSnackbar = null;
        }
    }

    private VerifyMerchantCallback mVerifyMerchantCallback = new VerifyMerchantCallback() {

        @Override
        public void onSuccess(VerifiedMerchantInfo verifiedMerchantInfo) {
            setStatus(getString(R.string.authenticated));
            initTransaction();
        }

        @Override
        public void onError(SpException exception) {
            clearPrompt();
            showError(Converter.from(exception));
        }
    };

    private TerminalScanningCallback mScanningCallback = new TerminalScanningCallback() {
        @Override
        public void onDeviceListRefresh(Collection<SpTerminal> devices) {
            List<SpTerminal> al = new ArrayList<>(devices);
            Collections.sort(al, new Comparator<SpTerminal>() {
                @Override
                public int compare(SpTerminal lhs, SpTerminal rhs) {
                    return lhs.getName().compareToIgnoreCase(rhs.getName());
                }
            });
            displayBluetoothDevice(al);
        }

        @Override
        public void onScanStopped(String message) {
            setMessage(message);
        }

        @Override
        public void onScanTimeout(String message) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(BLUETOOTH_FRAGMENT_TAG);
            if (fragment != null && fragment instanceof BluetoothDialogFragment) {
                ((BluetoothDialogFragment) fragment).setIsScanning(false);
            }
        }

        @Override
        public void onEnablingBluetooth(String message) {
            setMessage(message);
        }

        @Override
        public void onBluetoothPermissionDenied(String[] permissions) {

        }
    };

    private TransactionResultDialogFragment.OnPositiveButtonListener mResultDialogListener = new TransactionResultDialogFragment.OnPositiveButtonListener() {
        @Override
        public void onPositiveButtonClicked(DialogInterface dialog) {
            finish();
        }
    };

    private TransactionCallback mTransactionCallback = new TransactionCallback() {
        @Override
        public void onProgressTextUpdate(String message) {
            setMessage(message);
        }

        @Override
        public void onDeviceNotFound() {
            setMessage(getString(R.string.device_not_found));
        }

        @Override
        public void onDeviceConnected(SpTerminal spTerminal) {
            mProgressBar.setIndicatorColor(getResources().getColor(R.color.success_green));
            if (spTerminal != null) {
                if (mRememberDevice && spTerminal.getName() != null) {
                    PreferenceHelper.setString(MainActivity.this, R.string.pref_key_preferred_device, spTerminal.getName());
                }
                DeviceUsageTracker.getInstance(MainActivity.this).setLastUsed(spTerminal.getName(), new Date()).save();
                mDevice.setText(spTerminal.getName());
                mDevice.setVisibility(View.VISIBLE);
            }
            setStatus(getString(R.string.connected));
        }

        @Override
        public void onDeviceDisconnected(SpTerminal spTerminal) {
            setMessage(getString(R.string.disconnected, spTerminal.getName()));
        }

        @Override
        public void onBatteryStatus(SmartPesa.BatteryStatus batteryStatus) {
            @DrawableRes int bIcon = 0;
            @StringRes int bStatus = 0;
            if (batteryStatus == SmartPesa.BatteryStatus.LOW) {
                bIcon = R.drawable.ic_device_battery_low;
                bStatus = R.string.battery_low;
            } else if (batteryStatus == SmartPesa.BatteryStatus.CRITICALLY_LOW) {
                bIcon = R.drawable.ic_device_battery_critically_low;
                bStatus = R.string.battery_critically_low;
            }

            if (bIcon != 0) {
                mBatteryStatus.setCompoundDrawablesWithIntrinsicBounds(bIcon, 0, 0, 0);
            }

            if (bStatus != 0) {
                mBatteryStatus.setText(bStatus);
            }
        }

        @Override
        public void onShowSelectApplicationPrompt(List<String> appList) {
            setMessage(getString(R.string.select_app_prompt));
            mServiceManager.selectApplication(0); //TODO prompt for user selection
        }

        @Override
        public void onWaitingForCard(String message, SmartPesa.CardMode cardMode) {
            setMessage(getString(R.string.wait_for_card, cardMode));
            showPrompt(message);
        }

        @Override
        public void onShowInsertChipAlertPrompt() {
            setMessage(getString(R.string.chip_prompt));
        }

        @Override
        public void onCardDetected(SmartPesa.Method method) {
            clearPrompt();
            setStatus(method.name());
            mCardView.setMethod(method);
            setMessage(getString(R.string.card_detected));
        }

        @Override
        public void onReadCard(Card card) {
            mCardView.setCard(card);
            mCardView.setVisibility(View.VISIBLE);
        }

        @Override
        public void onReturnBin(Bin bin) {
            mCardView.setBin(bin);
        }

        @Override
        public void onShowPinAlertPrompt() {
            showPrompt(getString(R.string.enter_pin));
        }

        @Override
        public void onPinEntered() {
            clearPrompt();
        }

        @Override
        public void onShowInputPrompt() {
            setMessage(getString(R.string.input_prompt));
        }

        @Override
        public void onReturnInputStatus(SmartPesa.InputStatus inputStatus, @Nullable String input) {
            setMessage(getString(R.string.input_status) + input);
            if (inputStatus == SmartPesa.InputStatus.ENTERED) {
                mServiceManager.confirmInput(input);
            }
        }

        @Override
        public void onShowConfirmAmountPrompt() {
            showPrompt(getString(R.string.confirm_amount));
        }

        @Override
        public void onAmountConfirmed(boolean isConfirmed) {
            clearPrompt();
            if (isConfirmed) {
                setMessage(getString(R.string.amount_confirmed));
            }
        }

        @Override
        public void onTransactionFinished(SmartPesa.TransactionType transactionType, boolean isSuccess, @Nullable Transaction transaction, @Nullable SmartPesa.Verification verification, @Nullable SpTransactionException exception) {
            // Cleanup animation
            clearPrompt();
            mProgressBar.smoothToHide();

            // Prepare results
            Intent intent = new Intent();
            TransactionResult result = null;

            // Extract transaction result from SDK and convert to Parcelable Result
            if (transaction != null && transaction.getTransactionResult() != null && transaction.getTransactionResult().getTransactionId() != null) {
                result = Converter.from(transaction.getTransactionResult());
            }

            TransactionResultDialogFragment tf;
            int activityResult = RESULT_CANCELED;
            if (isSuccess) {
                setStatus(getString(R.string.success));

                activityResult = RESULT_OK;

                tf = TransactionResultDialogFragment.newInstance(result);

                intent.putExtra(com.smartpesa.intent.SmartPesa.KEY_TRANSACTION_RESULT, result);
            } else {
                setStatus(getString(R.string.failed));

                // Attach error to the intent
                TransactionException error = Converter.from(exception);
                TransactionError transactionError = TransactionError.builder()
                        .transactionException(error)
                        .transactionResult(result)
                        .build();
                intent.putExtra(com.smartpesa.intent.SmartPesa.KEY_TRANSACTION_ERROR, transactionError);

                tf = TransactionResultDialogFragment.newInstance(error, getFinalTransactedAmount(), result);
            }

            if (result != null) {
                setMessage(result.reference());
            } else if (exception != null) {
                setMessage(exception.getMessage());
            }

            // Set activity result
            setResult(activityResult, intent);

            showResultDialog(tf);
        }

        @Override
        public void onError(SpException exception) {
            mProgressBar.smoothToHide();
            setStatus(getString(R.string.error));
            setMessage(exception.getMessage());
            showError(Converter.from(exception));
        }

        @Override
        public void onStartPostProcessing(String providerName, Transaction transaction) {
            setMessage(getString(R.string.start_post_processing_prompt) + providerName);
        }

        @Override
        public void onReturnLoyaltyBalance(Loyalty loyalty) {
            setMessage("Loyalty return"); //TODO display the balance as completed?
        }

        @Override
        public void onShowLoyaltyRedeemablePrompt(LoyaltyTransaction loyaltyTransaction) {
            LoyaltyRedemptionDialogFragment fragment = LoyaltyRedemptionDialogFragment.newInstance(loyaltyTransaction);
            fragment.show(getSupportFragmentManager(), LOYALTY_REDEMPTION_FRAGMENT_TAG);
        }

        @Override
        public void onLoyaltyCancelled() {
            setMessage(getString(R.string.loyalty_canceled));
        }

        @Override
        public void onLoyaltyApplied(LoyaltyTransaction loyaltyTransaction) {
            mLoyaltyTransaction = loyaltyTransaction;
            setMessage(getString(R.string.loyalty_applied));
            mOriginalAmount.setText(mMoneyFormatHelper.format(mTransactionArgument.amount()));
            mOriginalAmount.setVisibility(View.VISIBLE);
            mAmount.setText(mMoneyFormatHelper.format(loyaltyTransaction.getAdjustedAmount()));
        }
    };

    private void showResultDialog(TransactionResultDialogFragment tf) {
        tf.setOnPositiveButtonListener(mResultDialogListener);
        tf.show(getSupportFragmentManager(), TRANSACTION_RESULT_DIALOG_TAG);
    }

    private void showPrompt(String prompt) {
        mSnackbar = Snackbar.make(mContainer, prompt, Snackbar.LENGTH_INDEFINITE);
        mSnackbar.setAction(R.string.dismiss, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSnackbar != null) {
                    mSnackbar.dismiss();
                }
            }
        });
        mSnackbar.show();
    }

    @Override
    public void onContinue(List<Redeemable> redeemedList) {
        mServiceManager.updateLoyaltyRedeemable(redeemedList);
    }

    @Override
    public void onCancel() {
        mServiceManager.updateLoyaltyRedeemable(null);
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    // Permission already asked at onCreate
    @SuppressWarnings("MissingPermission")
    @Override
    public void onConnected(@Nullable Bundle connectionHint) {
        Log.d(TAG, "onConnected() called with: " + "connectionHint = [" + connectionHint + "]");
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (lastLocation != null) {
            updateLocation(lastLocation);
        }
        startLocationUpdates();
    }

    // Permission already asked at onCreate
    @SuppressWarnings("MissingPermission")
    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    private void updateLocation(Location location) {
        Log.d(TAG, "updateLocation() called with: " + "location = [" + location.getLatitude() + "," + location.getLongitude() + "]");
        mServiceManager.setCoordinates(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "onConnectionSuspended() called with: " + "cause = [" + cause + "]");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed() called with: " + "connectionResult = [" + connectionResult + "]");
    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation(location);
    }

    private class DeviceSelectedListenerImpl implements BluetoothDialogFragment.DeviceSelectedListener<SpTerminal> {
        @Override
        public void onSelected(SpTerminal device, boolean shouldRemember) {
            setStatus(getString(R.string.device_selected));

            // Store selected preference, because we would like to save the preference only when the
            // selected device is valid and we're able to connect to the device.
            mRememberDevice = shouldRemember;
            doTransact(device);
        }
    }
}
